package br.edu.ifpr.android.l1e3alomundotoast;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AloMundoToastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_mundo_toast);


        /*Toast alo_mundo = Toast.makeText(this, getString(R.string.alo_mundo), Toast.LENGTH_SHORT);
        alo_mundo.setGravity(Gravity.RIGHT | Gravity.TOP, 0, 0);
        alo_mundo.show();*/

        criarToast();


    }

    private void criarToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));

        TextView text = (TextView) layout.findViewById(R.id.alo_mundo);
        text.setText(getString(R.string.alo_mundo));

        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
}